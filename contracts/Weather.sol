pragma solidity ^0.4.4;

contract Token {

    function totalSupply() constant public returns (uint256) {}
    function balanceOf(address _owner) constant public returns (uint256) {}
    function transfer(address _to, uint256 _value) public returns (bool success) {}
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {}
    function approve(address _spender, uint256 _value) public returns (bool success) {}
    function allowance(address _owner, address _spender) public constant returns (uint256 remaining) {}

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    
}



contract StandardToken is Token {
    

    function transfer(address _to, uint256 _value) public returns (bool success) {
      
        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
       
        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function balanceOf(address _owner) view public returns (uint256) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) constant public returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
    uint256 public totalSupply;
}


contract WeatherERC20Token is StandardToken {
  
  

    string public name;                   
    uint8 public decimals;             
    string public symbol;               
    address[] public buyers;
    uint256 public sellPrice;
    uint256 public buyPrice;

    function WeatherERC20Token() public payable {
        balances[msg.sender] = 100 ;            
        totalSupply = 100 ;                     
        name = "Weather Token";                                  
        decimals = 0;                         
        symbol = "ETH";  
        buyPrice = 1; 

    }
     /* Internal transfer, only can be called by this contract */
    function _transfer(address _from, address _to, uint _value) internal {
        require (_to != 0x0);                               // Prevent transfer to 0x0 address. Use burn() instead
        require (balances[_from] >= _value);               // Check if the sender has enough
        require (balances[_to] + _value > balances[_to]); // Check for overflows
        //require(!frozenAccount[_from]);                     // Check if sender is frozen
        //require(!frozenAccount[_to]);                       // Check if recipient is frozen
        balances[_from] -= _value;                         // Subtract from the sender
        balances[_to] += _value;                           // Add the same to the recipient
        Transfer(_from, _to, _value);
    }

    function approveAndCall(address _spender, uint256 _value, bytes _extraData) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);

        if(!_spender.call(bytes4(bytes32(keccak256("receiveApproval(address,uint256,address,bytes)"))), msg.sender, _value, this, _extraData)) { revert(); }
        return true;
    }

    function buy(uint _value) public payable returns (uint amount){
        amount = buyPrice;                    // calculates the amount
        require(balances[this] >= amount);               // checks if it has enough to sell
        balances[msg.sender] += amount;                  // adds the amount to buyer's balance
        balances[this] -= amount;                        // subtracts amount from seller's balance
        balances[this] = _value; 
        Transfer(this, msg.sender, amount);               // execute an event reflecting the change
        return amount;                                    // ends function and returns
    }

    function getBuyers() public view returns (address[]) {
        return buyers;
    }
}
