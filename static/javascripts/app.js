App = {
  web3Provider: null,
  contracts: {},

  /*init: function () {
    // Load pets.
    $.getJSON('../pets.json', function (data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);

        petsRow.append(petTemplate.html());
      }
    });

    return App.initWeb3();
  },*/

  initWeb3: function () {
    // Is there an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // If no injected web3 instance is detected, fall back to Ganache
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function () {
    $.getJSON('../build/contracts/WeatherERC20Token.json', function (data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var WeatherArtifact = data;
      App.contracts.WeatherERC20Token = TruffleContract(WeatherArtifact);

      // Set the provider for our contract
      App.contracts.WeatherERC20Token.setProvider(App.web3Provider);

      // Use our contract to retrieve and mark the adopted pets
      return App.markPaid();
    });

    return App.bindEvents();
  },

  bindEvents: function () {
    $(document).on('click', '.btn-transferFrom', App.payForToken);
    $(document).on('click', '.btn-approve', App.approveTransfer);
    $(document).on('click', '.btn-getToken', App.payForToken);
  },

  markPaid: function (adopters, account) {
    var payInstance;

    App.contracts.WeatherERC20Token.deployed().then(function (instance) {
      payInstance = instance;

      return payInstance.getBuyers.call();
    }).then(function (buyers) {
      for (i = 0; i < buyers.length; i++) {
        if (buyers[i] !== '0x0000000000000000000000000000000000000000') {
          $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
        }
      }
    }).catch(function (err) {
      console.log(err.message);
    });
  },


  getBalance: function (event) {
    event.preventDefault();

    var petId = parseInt($(event.target).data('id'));

    var payInstance;

    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.WeatherERC20Token.deployed().then(function (instance) {
        payInstance = instance;
        var approved;
        
        return payInstance.balanceOf.call(account);
      }).then(function (result) {
        console.log(result.toNumber());
        var accountAddress = $("#address").val();
        $.ajax({
          type: 'GET',
          url: 'http://localhost:3000/users/tokenValidity/'+accountAddress
        });

        return App.markPaid();
      }).catch(function (err) {
        console.log(err.message);
      });
    });

  },

  approveTransfer: function () {
    
    var payInstance;

    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = "0x7Ca58F260d7a84FBe9ECa5575679a9a1b0a87B4b";
      var account2 = "0x19E2f6F3BA1b25F830D32f04F73A23Eb037A102F";
      App.contracts.WeatherERC20Token.deployed().then(function (instance) {
        payInstance = instance;
        var approved;
       
        return payInstance.approve(account2, 10,
        {from: account});
      }).then(function (result) {
        console.log("Transaction successful!");
        return App.markPaid();
      }).catch(function (err) {
        console.log(err.message);
      });
    });

  },

  payForToken: function (event) {
    event.preventDefault();

    var payInstance;

    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = "0x7Ca58F260d7a84FBe9ECa5575679a9a1b0a87B4b";
      var account2 = "0x19E2f6F3BA1b25F830D32f04F73A23Eb037A102F";
     console.log(account);
      App.contracts.WeatherERC20Token.deployed().then(function (instance) {
        payInstance = instance;
        
        function readCookie(name) {
					var nameEQ = name + "=";
					var ca = document.cookie.split(';');
					for (var i = 0; i < ca.length; i++) {
						var c = ca[i];
						while (c.charAt(0) == ' ') c = c.substring(1, c.length);
						if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
					}
					return null;
        }
        
				if (readCookie('user_sid') == null) {
					$('.btn-getToken').on('click',function(){
						alert('Please sign in to be able to get a token');
					});
				} else {
          var accountAddress = $("#address").val();
          $.ajax({
            type: 'GET',
            url: 'http://localhost:3000/users/tokenValidity/'+accountAddress
          });
         return payInstance.transferFrom(account,account2, 1, {from: account2});
				}
          
      }).then(function (result) {
       
        return App.markPaid();
      }).catch(function (err) {
        console.log(err.message);
      });
    });

  }

};


(function($, document, window){
	
	$(document).ready(function(){

		// Cloning main navigation for mobile menu
		$(".mobile-navigation").append($(".main-navigation .menu").clone());

		// Mobile menu toggle 
		$(".menu-toggle").click(function(){
			$(".mobile-navigation").slideToggle();
		});

		var map = $(".map");
		var latitude = map.data("latitude");
		var longitude = map.data("longitude");
		if( map.length ){
			
			map.gmap3({
				map:{
					options:{
						center: [latitude,longitude],
						zoom: 15,
						scrollwheel: false
					}
				},
				marker:{
					latLng: [latitude,longitude],
				}
			});
			
		}
	});

	

})(jQuery, document, window);

$(window).load(function(req, res){

  App.initWeb3();
   
  
});