import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Outfit from '@/components/Outfit'
import Home from '@/components/Home'
import Map from '@/components/Map'
import weatherData from '@/components/weatherStations/weather'
import OutfitAccess from '@/components/OutfitAccess'
import Login from '@/components/Login'
import VueLocalStorage from "vue-localstorage";
import OutfitSuggestionDetail from '@/components/OutfitSuggestionDetails'
import OutfitSuggestion from '@/components/OutfitSuggestion'
import swal from 'sweetalert2'
import UserProfile from '@/components/UserProfile'
import UserProfileEdit from '@/components/UserProfileEdit'

Vue.use(Router)
Vue.use(VueLocalStorage);

export default new Router({
    routes: [{
        path: '/',
        name: 'HelloWorld',
        component: HelloWorld
    },
    {
        path: '/home',
        name: 'home',
        component: Home
    },
    {
        path: '/outfit',
        name: 'outfit',
        component: Outfit,
        beforeEnter: (to, from, next) => {


            if (Vue.localStorage.get("user") == null) {
                swal({
                    type: "error",
                    title: "Oops...",
                    html: "<b>Please Login to be able to access outfit adviser services!</b>"
                    //footer: "<a href>Why do I have this issue?</a>"
                });
                console.log('log in please');
                next(false);
            } else if (Vue.localStorage.get("user") != null) {
                var tokenDate;
                var accountAddress;
                var tokenDuration;
                var dateNow = new Date(Date.now());
                const data = JSON.parse(Vue.localStorage.get("user"));
                console.log(data);
                var array = [];
                array.push(data);
                array.forEach(element => {
                    tokenDate = new Date(element.data.tokenDate);
                    accountAddress = element.data.accountAddress;
                    tokenDuration = element.data.tokenDuration;
                    // console.log(element.data.tokenDate);
                });
                if (accountAddress == undefined) {
                    swal({
                        type: "error",
                        title: "Oops...",
                        html: "<b>Please purchase a token to access outfit adviser services!</b>"
                    });
                } else if (accountAddress != undefined && tokenDuration == 7) {

                    if ((dateNow.getDate() - tokenDate.getDate()) <= 7) {
                        console.log("OK 7");
                        next();
                    } else {
                        swal({
                            type: "error",
                            title: "Oops...",
                            html: "<b>Your token has expired please buy another one to access our outfit services</b>"
                        });
                        next(false);
                    }

                } else if (accountAddress != undefined && tokenDuration == 30) {

                    if ((dateNow.getDate() - tokenDate.getDate()) <= 30) {
                        console.log("OK 30");
                        next();
                    }

                } else if (accountAddress != undefined && tokenDuration == 90) {
                    if ((dateNow.getDate() - tokenDate.getDate()) <= 90) {
                        console.log("OK 90");
                        next();
                    }
                }
            }
        }
    },
    {
        path: '/weather/weatherData',
        name: 'weatherData',
        component: weatherData,
        beforeEnter: (to, from, next) => {


            if (Vue.localStorage.get("user") == null) {
                swal({
                    type: "error",
                    title: "Oops...",
                    html: "<b>Please Login to be able to access this service!</b>"
                    //footer: "<a href>Why do I have this issue?</a>"
                });
                console.log('log in please');
                next(false);
            } else if (Vue.localStorage.get("user") != null) {
                var tokenDate;
                var accountAddress;
                var tokenDuration;
                var dateNow = new Date(Date.now());
                const data = JSON.parse(Vue.localStorage.get("user"));
                console.log(data);
                var array = [];
                array.push(data);
                array.forEach(element => {
                    tokenDate = new Date(element.data.tokenDate);
                    accountAddress = element.data.accountAddress;
                    tokenDuration = element.data.tokenDuration;
                    // console.log(element.data.tokenDate);
                });
                if (accountAddress == undefined) {
                    swal({
                        type: "error",
                        title: "Oops...",
                        html: "<b>Please purchase a token to access this service!</b>"
                    });
                } else if (accountAddress != undefined && tokenDuration == 7) {

                    if ((dateNow.getDate() - tokenDate.getDate()) <= 7) {
                        console.log("OK 7");
                        next();
                    } else {
                        swal({
                            type: "error",
                            title: "Oops...",
                            html: "<b>Your token has expired please buy another one to access this service</b>"
                        });
                        next(false);
                    }

                } else if (accountAddress != undefined && tokenDuration == 30) {

                    if ((dateNow.getDate() - tokenDate.getDate()) <= 30) {
                        console.log("OK 30");
                        next();
                    }

                } else if (accountAddress != undefined && tokenDuration == 90) {
                    if ((dateNow.getDate() - tokenDate.getDate()) <= 90) {
                        console.log("OK 90");
                        next();
                    }
                }
            }
        }
    }, 
      
        {
            path: '/outfitaccess',
            name: 'outfitaccess',
            component: OutfitAccess,
            beforeEnter: (to, from, next) => {
                if (Vue.localStorage.get("user") == null) {
                    swal({
                        type: "error",
                        title: "Oops...",
                        html: "<b>Please Login to be able to buy a token!</b>"
                    });
                } else if (Vue.localStorage.get("user") != null) {
                    var tokenDate;
                    var accountAddress;
                    var tokenDuration;
                    var dateNow = new Date(Date.now());
                    const data = JSON.parse(Vue.localStorage.get("user"));
                    console.log(data);
                    var array = [];
                    array.push(data);
                    array.forEach(element => {
                        tokenDate = new Date(element.data.tokenDate);
                        accountAddress = element.data.accountAddress;
                        tokenDuration = element.data.tokenDuration;
                        // console.log(element.data.tokenDate);
                    });
                    if (accountAddress != undefined) {

                        if (accountAddress != undefined && tokenDuration == 7) {

                            if ((dateNow.getDate() - tokenDate.getDate()) <= 7) {
                                var days = 7 - (dateNow.getDate() - tokenDate.getDate());
                                swal({
                                    title: '<u>You have already a token</u>',
                                    type: 'info',
                                    html: '<b>Your token will expire after </b>' + days + '<b> days</b>',
                                    showCloseButton: true,
                                    focusConfirm: false,
                                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
                                    confirmButtonAriaLabel: 'Thumbs up, great!',
                                });
                                next(false);
                            } else {
                                next();
                            }

                        } else if (accountAddress != undefined && tokenDuration == 30) {

                            if ((dateNow.getDate() - tokenDate.getDate()) <= 30) {
                                var days = 30 - (dateNow.getDate() - tokenDate.getDate());
                                swal({
                                    title: '<u>You have already a token</u>',
                                    type: 'info',
                                    html: '<b>Your token will expire after </b>' + days + '<b> days</b>',
                                    showCloseButton: true,
                                    focusConfirm: false,
                                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
                                    confirmButtonAriaLabel: 'Thumbs up, great!',
                                });
                                next(false);
                            } else {
                                next();
                            }

                        } else if (accountAddress != undefined && tokenDuration == 90) {
                            if ((dateNow.getDate() - tokenDate.getDate()) <= 90) {
                                var days = 90 - (dateNow.getDate() - tokenDate.getDate());
                                swal({
                                    title: '<u>You have already a token</u>',
                                    type: 'info',
                                    html: '<b>Your token will expire after </b>' + days + '<b> days</b>',
                                    showCloseButton: true,
                                    focusConfirm: false,
                                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
                                    confirmButtonAriaLabel: 'Thumbs up, great!',
                                });
                                next(false);
                            } else {
                                next();
                            }
                        }
                    } else if (accountAddress == undefined) {
                        next();
                    }
                }
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        }, {
            path: '/OutfitSuggestion',
            name: 'OutfitSuggestion',
            component: OutfitSuggestion,
            beforeEnter: (to, from, next) => {


                if (Vue.localStorage.get("user") == null) {
                    swal({
                        type: "error",
                        title: "Oops...",
                        html: "<b>Please Login to be able to access outfit adviser services!</b>"
                            //footer: "<a href>Why do I have this issue?</a>"
                    });
                    console.log('log in please');
                    next(false);
                } else if (Vue.localStorage.get("user") != null) {
                    var tokenDate;
                    var accountAddress;
                    var tokenDuration;
                    var dateNow = new Date(Date.now());
                    const data = JSON.parse(Vue.localStorage.get("user"));
                    console.log(data);
                    var array = [];
                    array.push(data);
                    array.forEach(element => {
                        tokenDate = new Date(element.data.tokenDate);
                        accountAddress = element.data.accountAddress;
                        tokenDuration = element.data.tokenDuration;
                        // console.log(element.data.tokenDate);
                    });
                    if (accountAddress == undefined) {
                        swal({
                            type: "error",
                            title: "Oops...",
                            html: "<b>Please purchase a token to access outfit adviser services!</b>"
                        });
                    } else if (accountAddress != undefined && tokenDuration == 7) {

                        if ((dateNow.getDate() - tokenDate.getDate()) <= 7) {
                            console.log("OK 7");
                            next();
                        } else {
                            swal({
                                type: "error",
                                title: "Oops...",
                                html: "<b>Your token has expired please buy another one to access our outfit services</b>"
                            });
                            next(false);
                        }

                    } else if (accountAddress != undefined && tokenDuration == 30) {

                        if ((dateNow.getDate() - tokenDate.getDate()) <= 30) {
                            console.log("OK 30");
                            next();
                        }

                    } else if (accountAddress != undefined && tokenDuration == 90) {
                        if ((dateNow.getDate() - tokenDate.getDate()) <= 90) {
                            console.log("OK 90");
                            next();
                        }
                    }
                }
            }
        },
        {
            path: '/OutfitSuggestionDetail',
            name: 'OutfitSuggestionDetail',
            component: OutfitSuggestionDetail
        },
        {
            path: '/UserProfile/:id',
            name: 'UserProfile',
            component: UserProfile
        },
        {
            path: '/UserProfileEdit',
            name: 'UserProfileEdit',
            component: UserProfileEdit
        }
    ,{
        path: '/map',
        name: 'Map',
        component: Map
    }
    ]
})