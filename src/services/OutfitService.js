import Api from '@/services/Api'

export default {
  outfitService () {
     return Api().get('outfit') 
  }
}
