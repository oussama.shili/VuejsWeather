// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from "axios";
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps,{
    load:{
        key:'AIzaSyDcKhPLJ6wFKSiC9jkS9goMOipBdgXmPRc',
        libraries:'places'

    }
}

)
import {Tabs, Tab} from 'vue-tabs-component';

Vue.component('tabs', Tabs);
Vue.component('tab', Tab);
//import VueSweetalert2 from 'vue-sweetalert2';
import VModal from 'vue-js-modal'
import VeeValidate from 'vee-validate';

Vue.use(VModal)
Vue.use(require('vue-script2'), {
    components: {
      VueGoogleMaps
    }
  })
    //Vue.use(VueSweetalert2);
Vue.use(VeeValidate);

Vue.config.productionTip = false
    //axios.defaults.withCredentials = true
    /* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})
Vue.filter('temperature', function (value) {
    if (!value) return ''
    value = (value - 32) * 5 / 9;
    value = Math.round(value)
    return value + '\xB0'
  })
  
  Vue.filter('percent', function (value) {
    if (!value) return ''
    value = value * 100
    value = Math.round(value)
    return value + '%'
  })
  
  Vue.filter('round', function (value) {
    if (!value) return ''
    value = Math.round(value)
    return value
  })
  
  Vue.filter('formatDate', function (value) {
    if (!value) return ''
    value = new Date(value*1000)
    value = moment(value).format('ddd DD')
    return value
  })
  
  Vue.filter('formatTime', function (value) {
    if (!value) return ''
    value = new Date(value*1000)
    value = moment(value).format('h:mm a')
    return value
  })